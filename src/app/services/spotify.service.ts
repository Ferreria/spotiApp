import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class SpotifyService {

  artist:any[] = [];
  urlSearch:string = "https://api.spotify.com/v1/search";
  urlArtistId:string = "https://api.spotify.com/v1/artists";

  constructor( private http:Http ) { 

  }



  getArtist( param:string) {

  	let query =`?q=${ param }&type=artist`;
  	let url = this.urlSearch + query;

  	return this.http.get( url )
  			.map ( res => {

  				// console.log(res.json().artists.items);

  				this.artist = res.json().artists.items;
  				console.log(this.artist);
  			//	return res.json().artists.items;

  			})

  }

  getArtistId( id:string ) {

  	let query =`/${ id }`;
  	let url = this.urlArtistId + query;

  	return this.http.get( url )
  			.map ( res => {

  			 	console.log(res.json());

  				return res.json();

  			})

  }

  getTopTracks( id:string ) {

  	let query=`/${ id }/top-tracks?country=US`;
  	let url = this.urlArtistId + query;

  	return this.http.get( url )
  			.map ( res => {

  			 	console.log(res.json().tracks);

  				return res.json().tracks;

  			})

  }  


}
